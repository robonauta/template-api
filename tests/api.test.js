// External libraries
const request = require("supertest");

// API module
const API = require("../src/api");

describe("The project", () => {
  let api, mockColl, mockDb, mongoClient, stanConn;
  const corsOptions = { origin: "*" };

  const secret = "SUPERSECRET";

  beforeEach(() => {
    mockColl = {
      insertOne: jest.fn(),
      findOne: jest.fn(),
      deleteOne: jest.fn(),
    };

    mockDb = { collection: () => mockColl };

    mongoClient = { db: () => mockDb };

    stanConn = { publish: jest.fn() };

    api = API(corsOptions, { mongoClient, stanConn, secret });
  });

  it("can use Jest", () => {
    expect(true).toBe(true);
  });

  it("can use Supertest", async () => {
    const response = await request(api).get("/");
    expect(response.status).toBe(200);
    expect(response.body).toBe("Hello, World!");
  });

  it("can use CORS", async () => {
    const response = await request(api).get("/");
    const cors_header = response.header["access-control-allow-origin"];
    expect(cors_header).toBe("*");
  });

  it("creates an user", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(201);
    expect(response.body.user).toBeDefined();
    expect(stanConn.publish).toHaveBeenCalled();
    expect(mockColl.findOne).toHaveBeenCalled();
    expect(mockColl.insertOne).not.toHaveBeenCalled();
  });

  it("allows a registered user to delete its account", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    mockColl.findOne.mockResolvedValueOnce({
      name: "foo",
      email: "foo@example.com",
      password: "12356abcde",
      id: uid,
    });
    const correctToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGVmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.DU55f1y8dGSJPWYXrHUUwU0zGc-N8FixQqontudI4RE";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("Authentication", `Bearer ${correctToken}`);

    expect(response.status).toBe(200);
    expect(mockColl.findOne).toHaveBeenCalled();
    expect(stanConn.publish).toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });

  it("blocks a registered user to delete anothers account", async () => {
    const uid = "608ef5cc069020a1d61d5380";
    const wrongToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwOGZmNWNjMDY5MDIwYTFkNjFkNTM4MCJ9.J7xckIJZlqkmZBomOG8CIBJPYYen7I8Mx3hUn1rVnWc";

    const response = await request(api)
      .delete(`/users/${uid}`)
      .set("Authentication", `Bearer ${wrongToken}`);

    expect(response.status).toBe(403);
    expect(mockColl.findOne).not.toHaveBeenCalled();
    expect(stanConn.publish).not.toHaveBeenCalled();
    expect(mockColl.deleteOne).not.toHaveBeenCalled();
  });

  it("does not create user with missing name", async () => {
    const pw = "123456foo";
    const newUser = {
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Request body had missing field name");
  });

  it("does not create user with missing email", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Request body had missing field email");
  });

  it("does not create user with missing password", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Request body had missing field password");
  });

  it("does not create user with missing passwordConfirmation", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(
      "Request body had missing field passwordConfirmation"
    );
  });

  it("does not create user with empty name", async () => {
    const pw = "123456foo";
    const newUser = {
      name: " ",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Request body had malformed field name");
  });

  it("does not create user with malformed email", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "@.foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe("Request body had malformed field email");
  });

  it("does not create user with password lower than 8 characters", async () => {
    const pw = "123456";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(
      "Request body had malformed field password"
    );
  });

  it("does not create user with password bigger than 32 characters", async () => {
    const pw =
      "123456fooaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: pw,
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(400);
    expect(response.body.error).toBe(
      "Request body had malformed field password"
    );
  });

  it("does not create user with divergent password confirmation", async () => {
    const pw = "123456foo";
    const newUser = {
      name: "Foo",
      email: "foo@example.com",
      password: pw,
      passwordConfirmation: 'oie',
    };

    const response = await request(api).post("/users").send(newUser);

    expect(response.status).toBe(422);
    expect(response.body.error).toBe("Password confirmation did not match");
  });
});
