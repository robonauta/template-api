/* Pedro Henrique Barbosa de Almeida
 * N.USP 10258793
 *
 * função "validateEmail" retirada daqui:
 * https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
 */
const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  const database = mongoClient.db("ep1");
  const users = database.collection("users");

  api.post("/users", function (req, res) {
    if (req.body.name == undefined) {
      res.status(400).json({ error: "Request body had missing field name" });
      return;
    }
    if (req.body.email == undefined) {
      res.status(400).json({ error: "Request body had missing field email" });
      return;
    }
    if (req.body.password == undefined) {
      res
        .status(400)
        .json({ error: "Request body had missing field password" });
      return;
    }
    if (req.body.passwordConfirmation == undefined) {
      res
        .status(400)
        .json({ error: "Request body had missing field passwordConfirmation" });
      return;
    }

    if (!validateName(req.body.name)) {
      res.status(400).json({ error: "Request body had malformed field name" });
      return;
    }
    if (!validateEmail(req.body.email)) {
      res.status(400).json({ error: "Request body had malformed field email" });
      return;
    }
    if (!validatePassword(req.body.password)) {
      res
        .status(400)
        .json({ error: "Request body had malformed field password" });
      return;
    }

    if (req.body.password != req.body.passwordConfirmation) {
      res.status(422).json({ error: "Password confirmation did not match" });
      return;
    }

    const id = uuid();
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    savetoDB();

    async function savetoDB() {
      try {
        var result = await users.findOne({ email: email });
      } finally {
        if (result == null) {
          stanConn.publish(
            "users",
            JSON.stringify({
              eventType: "UserCreated",
              entityId: uuid,
              entityAggregate: {
                name: name,
                email: email,
                password: password,
              },
            })
          );
          res.status(201).json({
            user: {
              id: id,
              name: name,
              email: email,
            },
          });
        } else {
          res.status(409).json({ error: "Duplicated user" });
        }
      }
    }
  });

  api.delete("/users/:uuid", function (req, res) {
    if (req.headers.authentication == undefined) {
      res.status(401).json({ error: "Access Token not found" });
    }

    const id = req.params.uuid;
    var tokenRecv = req.headers.authentication.split(" ")[1];

    jwt.verify(tokenRecv, secret, function (err, decoded) {
      if (err) {
        res.status(401).json({ error: "Access Token is not valid" });
      } else {
        if (decoded.id == id) {
          updateBD();
        } else {
          res.status(403).json({ error: "Access Token did not match User ID" });
        }
      }
    });

    async function updateBD() {
      try {
        var result = await users.findOne({ uuid: id });
      } finally {
        if (result != null) {
          stanConn.publish(
            "users",
            JSON.stringify({
              eventType: "UserDeleted",
              entityId: uuid,
              entityAggregate: {},
            })
          );
          res.status(200).json({ id: id });
        } else {
          res.status(404).json({ error: "User not found" });
        }
      }
    }
  });

  return api;
};

function validateName(name) {
  return name.replace(/\s/g, "").length > 0;
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function validatePassword(password) {
  if (password.length >= 8 && password.length <= 32) return true;
  else return false;
}
